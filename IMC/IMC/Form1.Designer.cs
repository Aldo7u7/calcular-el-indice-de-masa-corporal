﻿namespace IMC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pantalla1 = new System.Windows.Forms.TextBox();
            this.pantalla2 = new System.Windows.Forms.TextBox();
            this.pantalla3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(120, 231);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Calcular IMC";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ALTURA (Mts)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "PESO (Kg)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "IMC";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // pantalla1
            // 
            this.pantalla1.Location = new System.Drawing.Point(190, 77);
            this.pantalla1.Name = "pantalla1";
            this.pantalla1.Size = new System.Drawing.Size(100, 20);
            this.pantalla1.TabIndex = 4;
            this.pantalla1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // pantalla2
            // 
            this.pantalla2.Location = new System.Drawing.Point(190, 127);
            this.pantalla2.Name = "pantalla2";
            this.pantalla2.Size = new System.Drawing.Size(100, 20);
            this.pantalla2.TabIndex = 5;
            // 
            // pantalla3
            // 
            this.pantalla3.Location = new System.Drawing.Point(190, 169);
            this.pantalla3.Name = "pantalla3";
            this.pantalla3.Size = new System.Drawing.Size(100, 20);
            this.pantalla3.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 409);
            this.Controls.Add(this.pantalla3);
            this.Controls.Add(this.pantalla2);
            this.Controls.Add(this.pantalla1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox pantalla1;
        private System.Windows.Forms.TextBox pantalla2;
        private System.Windows.Forms.TextBox pantalla3;
    }
}

